#include "pch.h"
#include "utils.cpp"

using std::string;
using spdlog::details::file_helper;
using spdlog::details::log_msg;
using fmt::memory_buffer;
using fmt::format_to;

static const string FILE_NAME = "logs/file_helper.txt";
static const char* tested_logger_name = "null_logger";
static const char* tested_logger_name2 = "null_logger2";

static void write_file(file_helper& helper, size_t nFiles)
{
	memory_buffer buffer;
	format_to(buffer, "{}", string(nFiles, '1'));
	helper.write(buffer);
	helper.flush();
}

static void test_split_ext(const char* fname, const char* expect_base, const char* expect_ext)
{
	spdlog::filename_t filename(fname);
	spdlog::filename_t expected_base(expect_base);
	spdlog::filename_t expected_ext(expect_ext);

#ifdef _WIN32 // replace folder sep
	std::replace(filename.begin(), filename.end(), '/', '\\');
	std::replace(expected_base.begin(), expected_base.end(), '/', '\\');
#endif
	spdlog::filename_t basename, ext;
	std::tie(basename, ext) = file_helper::split_by_extension(filename);
	ASSERT_EQ(basename, expected_base);
	ASSERT_EQ(ext, expected_ext);
}

TEST(FileHelper, FileReopenTruncate)
{
	prepare_logdir();
	file_helper helper;
	helper.open(FILE_NAME);
	write_file(helper, 12);
	ASSERT_EQ(helper.size(), 12);
	helper.reopen(true);
	ASSERT_EQ(helper.size(), 0);
}

TEST(FileHelper, FileSize)
{
	prepare_logdir();
	size_t expected_size = 123;
	{
		file_helper helper;
		helper.open(FILE_NAME);
		write_file(helper, expected_size);
		ASSERT_EQ(static_cast<size_t>(helper.size()), expected_size);
	}
	ASSERT_EQ(get_filesize(FILE_NAME), expected_size);
}

TEST(FileHelper, FileExists)
{
	prepare_logdir();
	ASSERT_FALSE(file_helper::file_exists(FILE_NAME));
	file_helper helper;
	helper.open(FILE_NAME);
	ASSERT_TRUE(file_helper::file_exists(FILE_NAME));
}

TEST(FileHelper, FileReopen)
{
	prepare_logdir();
	size_t expected_size = 14;
	file_helper helper;
	helper.open(FILE_NAME);
	write_file(helper, expected_size);
	ASSERT_EQ(helper.size(), expected_size);
	helper.reopen(false);
	ASSERT_EQ(helper.size(), expected_size);
}

TEST(FileHelper, FileName)
{
	prepare_logdir();

	file_helper helper;
	helper.open(FILE_NAME);
	ASSERT_EQ(helper.filename(), FILE_NAME);
}

TEST(FileHelper, SplitExtension)
{
	test_split_ext("mylog.txt", "mylog", ".txt");
	test_split_ext(".mylog.txt", ".mylog", ".txt");
	test_split_ext(".mylog", ".mylog", "");
	test_split_ext("/aaa/bb.d/mylog", "/aaa/bb.d/mylog", "");
	test_split_ext("/aaa/bb.d/mylog.txt", "/aaa/bb.d/mylog", ".txt");
	test_split_ext("aaa/bbb/ccc/mylog.txt", "aaa/bbb/ccc/mylog", ".txt");
	test_split_ext("aaa/bbb/ccc/mylog.", "aaa/bbb/ccc/mylog.", "");
	test_split_ext("aaa/bbb/ccc/.mylog.txt", "aaa/bbb/ccc/.mylog", ".txt");
	test_split_ext("/aaa/bbb/ccc/mylog.txt", "/aaa/bbb/ccc/mylog", ".txt");
	test_split_ext("/aaa/bbb/ccc/.mylog", "/aaa/bbb/ccc/.mylog", "");
	test_split_ext("../mylog.txt", "../mylog", ".txt");
	test_split_ext(".././mylog.txt", ".././mylog", ".txt");
	test_split_ext(".././mylog.txt/xxx", ".././mylog.txt/xxx", "");
	test_split_ext("/mylog.txt", "/mylog", ".txt");
	test_split_ext("//mylog.txt", "//mylog", ".txt");
	test_split_ext("", "", "");
	test_split_ext(".", ".", "");
	test_split_ext("..txt", ".", ".txt");
}

TEST(Registry, RegisterDrop)
{
	spdlog::drop_all();
	spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name);
	ASSERT_NE(spdlog::get(tested_logger_name), nullptr);
	// Throw if registring existing name
	ASSERT_THROW(spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name), const spdlog::spdlog_ex);
}

TEST(Registry, ExplicitRegister)
{
	spdlog::drop_all();
	auto logger = std::make_shared<spdlog::logger>(tested_logger_name, std::make_shared<spdlog::sinks::null_sink_st>());
	spdlog::register_logger(logger);
	ASSERT_NE(spdlog::get(tested_logger_name), nullptr);
	// Throw if registring existing name
	ASSERT_THROW(spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name), const spdlog::spdlog_ex);
}

TEST(Registry, ApplyAll)
{
	spdlog::drop_all();
	auto logger = std::make_shared<spdlog::logger>(tested_logger_name, std::make_shared<spdlog::sinks::null_sink_st>());
	spdlog::register_logger(logger);
	auto logger2 = std::make_shared<spdlog::logger>(tested_logger_name2, std::make_shared<spdlog::sinks::null_sink_st>());
	spdlog::register_logger(logger2);

	int counter = 0;
	spdlog::apply_all([&counter](std::shared_ptr<spdlog::logger>) { counter++; });
	ASSERT_EQ(counter, 2);

	counter = 0;
	spdlog::drop(tested_logger_name2);
	spdlog::apply_all([&counter](std::shared_ptr<spdlog::logger> l) {
		ASSERT_EQ(l->name(), tested_logger_name);
		counter++;
		});
	ASSERT_EQ(counter, 1);
}

TEST(Registry, Drop)
{
	spdlog::drop_all();
	spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name);
	spdlog::drop(tested_logger_name);
	ASSERT_FALSE(spdlog::get(tested_logger_name));
}

TEST(Registry, DropDefault)
{
	spdlog::set_default_logger(spdlog::null_logger_st(tested_logger_name));
	spdlog::drop(tested_logger_name);
	ASSERT_FALSE(spdlog::default_logger());
	ASSERT_FALSE(spdlog::get(tested_logger_name));
}

TEST(Registry, DropAll)
{
	spdlog::drop_all();
	spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name);
	spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name2);
	spdlog::drop_all();
	ASSERT_FALSE(spdlog::get(tested_logger_name));
	ASSERT_FALSE(spdlog::get(tested_logger_name2));
	ASSERT_FALSE(spdlog::default_logger());
}

TEST(Registry, DropNonExisting)
{
	spdlog::drop_all();
	spdlog::create<spdlog::sinks::null_sink_mt>(tested_logger_name);
	spdlog::drop("some_name");
	ASSERT_FALSE(spdlog::get("some_name"));
	ASSERT_TRUE(spdlog::get(tested_logger_name));
	spdlog::drop_all();
}

TEST(Registry, DefaultLogger)
{
	spdlog::drop_all();
	spdlog::set_default_logger(spdlog::null_logger_st(tested_logger_name));
	ASSERT_TRUE(spdlog::get(tested_logger_name) == spdlog::default_logger());
	spdlog::drop_all();
}

TEST(Registry, DefaultLoggerNull)
{
	spdlog::set_default_logger(nullptr);
	ASSERT_FALSE(spdlog::default_logger());
}

TEST(Registry, DisableAutomaticRegistration)
{
	// set some global parameters
	spdlog::level::level_enum log_level = spdlog::level::level_enum::warn;
	spdlog::set_level(log_level);
	// but disable automatic registration
	spdlog::set_automatic_registration(false);
	auto logger1 = spdlog::create<spdlog::sinks::daily_file_sink_st>(tested_logger_name, "filename", 11, 59);
	auto logger2 = spdlog::create_async<spdlog::sinks::stdout_color_sink_mt>(tested_logger_name2);
	// loggers should not be part of the registry
	ASSERT_FALSE(spdlog::get(tested_logger_name));
	ASSERT_FALSE(spdlog::get(tested_logger_name2));
	// but make sure they are still initialized according to global defaults
	ASSERT_TRUE(logger1->level() == log_level);
	ASSERT_TRUE(logger2->level() == log_level);
	spdlog::set_level(spdlog::level::info);
	spdlog::set_automatic_registration(true);
}
